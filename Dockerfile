FROM azul/zulu-openjdk-alpine:8
ARG FE_SRV_JARNAME
WORKDIR /app/
RUN echo $FE_SRV_JARNAME
RUN apk update
RUN apk add nodejs
RUN apk add yarn
COPY devschool-front-app-server/build/libs/$FE_SRV_JARNAME devschool-front-app-server.jar
COPY entrypoint.sh entrypoint.sh
RUN chmod +x entrypoint.sh
EXPOSE 80
# CMD ["java","-jar","devschool-front-app-server.jar"]
CMD ./entrypoint.sh
